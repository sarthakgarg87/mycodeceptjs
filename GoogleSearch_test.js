Feature('GoogleSearch');


Scenario('Testing Google Search', async({ I, GooglePage }) => {
    I.amOnPage('/');
    GooglePage.verifyGoogleSearchPage();
    let title =  await I.grabTitle();  
    var assert = require('assert');
    assert.equal(title, 'Google'); 
    GooglePage.googleSearch('Flipkart'); 
  
});
