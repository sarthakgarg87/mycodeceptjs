const { I } = inject();

module.exports = {

  loginFields:{
    username: "//input[@class='_2IX_2- VJZDxU']",
    password: "//div/input[@type='password']",
  },

  loginButton: "//button/span[text()='Login']",
        profile: "//div[@class='_1psGvi _3BvnxG']",
        logoutButton: "//div[text()= 'Logout']",


// introducing methods
userLogin: function(email, password) {
  I.amOnPage("https://www.flipkart.com/");
  I.fillField(this.loginFields.username, email);
  I.fillField(this.loginFields.password, password);
  I.click(this.loginButton);
  I.wait(5);
},

userLogout: function(){
  I.click(this.profile);
  I.click(this.profile);
  I.wait(2);
  I.click(this.logoutButton);
  I.wait(5);

}

}
