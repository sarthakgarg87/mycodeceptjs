const { I } = inject();

module.exports = {

    googleSearchButton  : "(//input[@name='btnK'])[2]",
    googleSearchField: "//input[@name='q']",

verifyGoogleSearchPage: function(){
 I.amOnPage('/');
 I.seeElement(this.googleSearchButton);
 I.see('Google');
},

googleSearch: function(text){
    I.fillField(this.googleSearchField, text);
    I.click(this.googleSearchButton);
    I.wait(5);
}

}